import * as functions from 'firebase-functions';
import * as admin from "firebase-admin"

admin.initializeApp({
  credential: admin.credential.applicationDefault()
})

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const helloWorld = functions.https.onRequest((request, response) => {
  response.send("Hello from Firebase!");
});

export const viajeCreated = functions.firestore.document("viajes/{viajeId}").onCreate(async (snapshot: FirebaseFirestore.DocumentSnapshot, context: functions.EventContext) => {
  const newViaje = snapshot.data()
  console.log(`Nuevo viaje creado! [${JSON.stringify(newViaje)}]`);
  const token = "cPwhaAZWq8k:APA91bFWhVxHFr4uTXa3u1oLfeOnl3ef_zBhDLemqYf_J0tA4D3ViTMHcevxREviDLwxbzBhPjkqjZQF1NBPrV9mkb-o24iCtTct_lF5Wvr3nByUSzufP7NVa1IyIGq9FSLFRmfAIorg"
  const message = {
    notification: {
      title: "Nuevo viaje creado",
      body: "¡Se ha creado un nuevo viaje!"
    },
    token
  }

  try {
    await admin.messaging().send(message)
    console.log(`Notificación enviada correctamente al cliente [${token}]`);
  } catch (error) {
    console.log(`No se pudo enviar la notificación al cliente [${token}]`);
  }
})
